const {JSDOM} = require('jsdom');
const fetch = require('node-fetch');

const options = {
    runScripts: 'dangerously',
    resources: "usable"
};

JSDOM.fromURL("http://applicant-test.us-east-1.elasticbeanstalk.com/", options).then(async dom => {
    const document = dom.window.document;
    const MouseEvent = dom.window.MouseEvent;
    const form = document.querySelector('form');
    const button = document.querySelector("input[type='button']");
    var script = document.querySelector("script").src;
    var content = await getScript(script);

    var scriptEl = document.createElement('script');
    scriptEl.textContent = content;
    document.head.appendChild(scriptEl);

    let calls = 0;

    form.addEventListener('submit', event => {
        event.preventDefault()
        console.log('submitted', event);
        calls++
    });

    const clickEvent = new MouseEvent('click', {});
    button.dispatchEvent(clickEvent);
});


async function getScript(url) {
    return new Promise(async resolve => {
        const response = await fetch(url);
        const text = await response.text();
        resolve(text);
    });
}




