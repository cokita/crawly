const jsdom = require("jsdom");
const fetch = require('node-fetch');
var FormData = require('form-data');
var request = require('request');
const { JSDOM } = jsdom;

const options = {
    runScripts: 'dangerously',
    resources: "usable"
};

function getAnswer() {
    JSDOM.fromURL("http://applicant-test.us-east-1.elasticbeanstalk.com/", options).then(async dom => {
    const document = dom.window.document;
    console.log(document.cookie);
    const bodyEl = document.body;
    const form = bodyEl.querySelector("form");
    const button = document.querySelector("input[type='button']");
    const token = document.querySelector("input[name='token']");
    var script = document.querySelector("script").src;
    var content = await getScript(script);
    content = content.replace(',document.getElementById("form").submit()', '');

    console.log('Token: '+token.value);
    var scriptEl = document.createElement('script');
    scriptEl.textContent = content;
    document.head.appendChild(scriptEl);
        button.addEventListener('click', async function(){
            var newToken = document.querySelector("input[name='token']").value;
            console.log('Novo Token: '+newToken);
            var answer = await callForm(newToken, document.cookie);

        });
    //
    button.click();


}).catch(function (e) {
        console.log(e);
    });
}


async function getScript(url) {
    return new Promise(async resolve => {
        const response = await fetch(url);
        const text = await response.text();
        resolve(text);
    });
}

async function callForm(token, cookie){
    var j = request.jar();
    var cookies = request.cookie(cookie);
    j.setCookie(cookies, 'applicant-test.us-east-1.elasticbeanstalk.com');
    var form = {'token': token};
    const optionsReq = {
        method: 'POST',
        url: 'http://applicant-test.us-east-1.elasticbeanstalk.com',
        jar: j,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(JSON.stringify(form),'utf8'),
        },
        Credentials: 'same-origin',
        formData: form
    };
    request(optionsReq, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log(response.rawHeaders);
            console.log(body);
        }
    });
}

getAnswer();

// async function callForm(token) {
//     var form = new FormData();
//         form.append('token', token);
//     console.log(form.values());
//     var hdr = {'Content-Type': 'application/x-www-form-urlencoded'};
//
//     return new Promise(async resolve => {
//         const response = await fetch('http://applicant-test.us-east-1.elasticbeanstalk.com', {
//             method: 'POST',
//             body:    form,
//             headers: hdr,
//         });
//         const text = await response.text();
//         console.log(text);
//
//     });
// }

// async function callForm(token){
//     return new Promise(async resolve => {
//         var form = new FormData();
//         form.append('token', token);
//         form.submit('http://applicant-test.us-east-1.elasticbeanstalk.com', function(err, res) {
//             console.log(res.resume());
//             res.resume();
//         });
//     });
// }


