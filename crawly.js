const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const dom = new JSDOM(``, {
    url: "http://applicant-test.us-east-1.elasticbeanstalk.com/",
    referrer: "http://applicant-test.us-east-1.elasticbeanstalk.com/",
    contentType: "text/html",
    includeNodeLocations: true,
    storageQuota: 10000000
});
const document = dom.window.document;
const bodyEl = document.body;
const form = bodyEl.querySelector("form");


console.log(form);