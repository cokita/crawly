var Crawler = require("crawler");

var c = new Crawler({
    maxConnections : 10,
    // This will be called for each crawled page
    callback : function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            var $ = res.$;
            // $ is Cheerio by default
            //a lean implementation of core jQuery designed specifically for the server

        }
        done();
    }
});

// Queue just one URL, with default callback
c.queue([{
    uri: 'http://applicant-test.us-east-1.elasticbeanstalk.com',
    jQuery: false,

    // The global callback won't be called
    callback: function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            console.log(res.body);
        }
        done();
    }
}]);
 